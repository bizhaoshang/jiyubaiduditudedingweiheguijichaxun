<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bugs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('platform_id')->comment('项目id');
            $table->string('error')->comment('系统的错误信息，异常信息');
            $table->string('remark')->comment('错误信息说明');
            $table->string('catched_at',20)->comment('异常捕获时间');
            $table->tinyInteger('source')->comment('消息来源');
            $table->tinyInteger('status_id')->unsigned('状态id')->comment();
            $table->string('url',100)->comment('发生错误的url');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bugs');
    }
}
