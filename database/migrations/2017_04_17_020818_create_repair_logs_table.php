<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->comment('修复人id');
            $table->integer('bug_id')->comment('bug id');
            $table->integer('bug_status')->comment('bug 状态');
            $table->string('remark')->comment('错误信息说明');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repair_logs', function (Blueprint $table) {
            //
        });
    }
}
