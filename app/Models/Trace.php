<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Trace extends Model
{
  

    /**
     * 不可被批量赋值的属性。
     *
     * @var array
     */
    protected $guarded = [];

    /**
     *属于该平台的负责人
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
