<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlatformUser extends Model
{
    //使用软删除
    use SoftDeletes;

    //设置黑名单
    protected $guarded = [];
}
