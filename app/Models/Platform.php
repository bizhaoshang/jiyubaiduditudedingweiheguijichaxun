<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Platform extends Model
{
    //使用软删除
    use SoftDeletes;

    /**
     * 不可被批量赋值的属性。
     *
     * @var array
     */
    protected $guarded = [];

    /**
     *属于该平台的负责人
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}
