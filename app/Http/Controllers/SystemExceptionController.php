<?php

namespace App\Http\Controllers;

use App\Models\Platform;
use Illuminate\Http\Request;
use App\Models\Mongodb\SystemException;

class SystemExceptionController extends Controller
{
    //项目列表
    public function index(Request $request)
    {
        $left  = "system";
        
        $query = SystemException::where([]);        
        $list  = $query->paginate(15);
        return view('systemexception/index',compact('list'))->with(compact('left'));
    }

    
    
    public function del($id)
    {
        $rs = SystemException::find($id);
        $rs->delete();
        return back();
        
    }

    public function info($id)
    {

        $info = SystemException::find($id);
        dd($info);
        
    }
}
