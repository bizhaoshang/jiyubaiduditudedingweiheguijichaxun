<?php

namespace App\Http\Controllers;

use App\Models\Platform;
use App\Models\PlatformsUser;
use App\Models\User;
use Illuminate\Http\Request;

class PlatformUserController extends Controller
{
    //项目负责人列表
    public function chargeList()
    {
        $left = "leadcharge";
        //查询所有项目
        $platforms = Platform::all();
        //查询所有负责人
        $users = User::all();

        return view('platform/leadchargelist',compact('platforms','users'))->with(compact('left'));
    }

    //添加项目指定负责人
    public function chargeAdd(Request $request)
    {
        $platform_id = $request->platId;
        $user_id = $request->chargeId;
        return app('platformUsers')->chargeAdd($platform_id,$user_id);
    }

    //项目对应负责人列表
    public function manageUser($id)
    {
        $left  = "leadcharge";
        $users = app('platformUsers')->manageUser($id);
        $platformName    = app('platformUsers')->platformName($id);
        $noPlatformUsers = app('platformUsers')->noPlatformUser($id);
        return view('platform/leadcharge',compact('users','platformName','noPlatformUsers'))->with(compact('left'));;
    }

    //删除项目指定负责人
    public function platformUserDel(Request $request)
    {
        $user_id = $request->user_id;
        $platform_id = $request->platform_id;
        return app('platformUsers')->platformUserDel($user_id,$platform_id);
    }
}
