<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Trace;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['appids'] = Trace::select('appid')->where(['u'=>Auth::user()->username])
                                ->groupby('appid')
                                ->get();
        return view('home',$data);
    }

    public function admin()
    {
        $data['appids'] = Trace::select('appid')->where(['u'=>Auth::user()->username])
                                ->groupby('appid')
                                ->get();
        return view('layouts.work',$data);
    }

    public function logout()
    {
        Auth::logout();
        return view('auth/login');
    }

    
}
