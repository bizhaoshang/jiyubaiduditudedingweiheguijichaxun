<?php

namespace App\Http\Controllers;

use Auth,Validator;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //负责人列表
    public function userList(Request $request)
    {
        $left = "leadcharge";
        $users = User::where([]);
        //搜索条件
        $keywords = $request->keywords;
//        dd($keywords);
        if($keywords){
            $users = $users->where("true_name","like","%$keywords%");
        }
        $users = $users->paginate(5);
        return view('platform/userlist',compact('users'))->with(compact('left'));
    }



   


    //负责人的编辑
    public function info(Request $request)
    {
        $user = Auth::user();
        return view('user/info',compact('user'));
    }

    //执行负责人编辑
    public function update(Request $request)
    {
        $user = Auth::user();
        $data = $request->except(['username','_token','password_confirmation']);
        //dd($data);
        $validator =  Validator::make($data, [
            
            'email' => 'required|email|max:255',
            'token' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return redirect('user/info')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        if( isset($data['password'])&& ($data['password']==$request->password_confirmation)) {
            $data['password'] = bcrypt($data['password']);           
            
        } else {
            return redirect('user/info')->with('msg', ['type'=>'warning','info'=>'密码错误']);  
        }
        User::where('id',$user->id)->update($data);
        return redirect('user/info')->with('msg', ['type'=>'success','info'=>'修改成功']);;
    }

}
