<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('bugs', 'App\Repositories\BugRepository');
        $this->app->singleton('platforms','App\Repositories\PlatformRepository');
        $this->app->singleton('platformUsers','App\Repositories\PlatformUserRepository');
        $this->app->singleton('users','App\Repositories\UserRepository');
        $this->app->singleton('email','App\Repositories\EmailRepository');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
