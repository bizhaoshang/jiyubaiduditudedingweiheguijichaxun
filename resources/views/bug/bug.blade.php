@extends('layouts.master')
@section('content')
    @push('css')
    <link rel="stylesheet" href="{{asset('/bugsystem/plugins/datatables/dataTables.bootstrap.css')}}">
    @endpush
    {{--主体右上--}}
    <section class="content-header">
        <h1 id="examples">
            bug 管理
            <small>advanced tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!--主体内容-->
    <section class="content">
        <div class="row" id="app">
            <!--列表-->
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">错误消息列表</h3>
                        {{--<div class="pull-left"><h3 class="box-title">@lang('leftnav.showTask')</h3></div>--}}
                        <!--搜索框-->
                        <div class="input-group  pull-right col-md-2">
          		        <span class="input-group-btn">
                            <button id="keyword" type="button" class="btn btn-info btn-flat">搜索</button>
                        </span>
                            <input type="text" class="form-control" name="seach"   placeholder="">
                        </div>
                        <!--搜索框-->
                        <div class="pull-right"style="margin-right: 15px;">
                            <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#add">添加</button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>项目名称</th>
                                <th>消息来源</th>
                                <th>捕获时间</th>
                                
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($bugs as $bug)
                                    <tr>
                                        <td>{{$bug->id or ''}}</td>
                                        <td>{{$bug->platform->name or ''}}</td>
                                        @if($bug->source=='1')
                                            <td>系统捕获</td>
                                            @else
                                            <td>手动输入</td>
                                        @endif
                                        <td>{{$bug->created_at or ''}}</td>                                 
                                       
                                        <td>{{$status[$bug->status]}}</td>
                                       
                                    
                                        <td><a href="" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete" data-id="{{$bug->id}}">删除</a>&nbsp<a href="{{url('bug/bug-detail',$bug->bug_code)}}" class="btn btn-info btn-xs">详情</a> &nbsp; <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit" data-id="{{$bug->id}}">修改</a> &nbsp </td>
                                    </tr>
                                @endforeach
                           
                            </tbody>
                        </table>
                        <div style="float:right;margin:0px;padding:0px;">
                            @if (!empty($keywords))
                                {{$bugs->appends(['keywords' => $keywords])->links()}}
                            @else
                                {{$bugs->links()}}
                            @endif
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <!--模态框-->
            <!--编辑-->
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">修改</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>确认完成</label>
                                        <select name="confirmBug" id="">
                                            <option value="0">未解决</option>
                                            <option value="1">已解决</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="editSave" style="width: 120px" data-id="">保存</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--编辑end-->
            <!--删除-->
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h2 class="modal-title" id="myModalLabel" style="color: red"><i class="fa fa-warning"></i>警告！</h2>
                        </div>
                        <div class="modal-body">
                           是否确定删除此条数据?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            <button type="button" id="delSave" class="btn btn-danger" data-dismiss="modal" style="width: 120px"  data-id="">删除</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--删除end-->
            <!--添加-->
            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">bug添加</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>项目名称</label>
                                        {{--<input type="text" class="add-bug" style="width: 100%" name="platform">--}}
                                        <select name="platform" id="">
                                            @foreach($platforms as $platform)
                                                <option value="{{$platform->id}}">{{$platform->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>错误类型</label>
                                        <input type="text" class="add-bug" style="width: 100%" name="error_type">
                                    </div>
                                    <div class="form-group">
                                        <label>错误文件位置</label>
                                        <input type="text" class="add-bug" style="width: 100%" name="file" >
                                    </div>
                                    <div class="form-group">
                                        <label>错误信息</label>
                                        <input type="text" class="add-bug" style="width: 100%" name="error" >
                                    </div>
                                    <div class="form-group">
                                        <label>错误详情</label>
                                        <textarea name="remark" id="remark" class="add-bug" style="width: 100%"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>URL</label>
                                        <input type="text" class="add-bug" style="width: 100%" name="url" >
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>  `
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="addSave" style="width: 120px">添加</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--添加end-->
            <!--操作完成后模态框-->
            <div id="example" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">操作结果</h4>
                        </div>

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="result">
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--操作完成后模态框end-->
        </div>
    </section>

    @push('scriptfiles')
    <script src="{{asset('/bugsystem/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/bugsystem/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
        <script>
            $(function(){
                /*$('#example1').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });*/

                $("#example1").DataTable({
                    "oLanguage" : {
                        "sLengthMenu": "每页显示 _MENU_ 条记录",
                        "sZeroRecords": "抱歉， 没有找到",
                        "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                        "sInfoEmpty": "没有数据",
                        "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                        "sZeroRecords": "没有检索到数据",
                        "sSearch": "搜索:",
                        "oPaginate": {
                            "sPrevious": "前一页",
                            "sNext": "后一页",
                        },
                    },
                    "aLengthMenu": [
                        [20, 30, 50, -1],
                        ["20条", "30条", "50条", "All"]
                    ],
                    "paging": false,
                    "searching":false,
                    "ordering":false,
                });

                //添加bug信息，表单验证
                $('.add-bug').on('blur',function(){
                    $(this).each(function(){
                        var _name = $(this).attr('name');
                        //url判断
                        if(_name=='url'){
                            var check = checkUrl($(this).val());
                             if (check==false) {
                                alert('链接地址必须是http://开头');
                                return false;
                             }
                             //验证url地址
                             function checkUrl(url) {
                                var strReg = "((http|ftp|https)://)([a-zA-Z0-9_-]+\.)*";
                                var re = new RegExp(strReg);
                                if(!re.test(url)) {
                                 return false;
                                } else {
                                return true;
                                }
                             }
                        }
                    });

                });

                //删除链接
                $('#delete').on('show.bs.modal', function (e) {
                    var tar = $(e.relatedTarget);
                    var id = $(tar).data('id');
                    $("#delSave").data('id', id);
                });
                $('#delSave').click(function () {
                    var id = $(this).data('id');
                    $.ajax({
                        type:'post',
                        url:"{{url('bug/bug-del')}}/" + id,
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        dataType:'json',
                        success: function (e) {
                            if (e.status==0) {
                                //将操作信息写入标签
                                $("#result").text(e.msg);
                                $('#example').modal('show');
                                $('#example').on('hide.bs.modal',function(){
                                    location.reload();
                                });
                            } else {
                                alert(e.msg);
                            }
                        },
                        error:function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log(errorThrown);
                        }
                    });
                });

                //编辑
                $('#edit').on('show.bs.modal', function(e){
                    var tar = $(e.relatedTarget);
                    var id = $(tar).data('id');
                    $("#editSave").data('id', id);
                    //获取指定错误的信息
                    {{--$.ajax({--}}
                        {{--type:'post',--}}
                        {{--url:"{{url('bug/bug-edit')}}/" + id,--}}
                        {{--headers: {--}}
                            {{--'X-CSRF-TOKEN': "{{csrf_token()}}"--}}
                        {{--},--}}
                        {{--dataType:'json',--}}
                        {{--success: function (e) {--}}
                            {{--if (e.status==0) {--}}
                                {{--$('input[name="editError"]').val(e.data.error);--}}
                                {{--$('input[name="editRemark"]').val(e.data.remark);--}}
                            {{--} else {--}}
                                {{--alert(e.msg);--}}
                            {{--}--}}
                        {{--},--}}
                        {{--error:function (XMLHttpRequest, textStatus, errorThrown) {--}}
                            {{--console.log(errorThrown);--}}
                        {{--}--}}
                    {{--});--}}
                });

                //执行编辑
                $('#editSave').click(function(){
                    var id = $(this).data('id');
                    var confirm = $('select[name="confirmBug"]').val();
                    $.ajax({
                        type:'post',
                        url:"{{url('bug/do-bug-edit')}}",
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        data:{confirm:confirm, id:id},
                        dataType:'json',
                        success: function (e) {
                            if (e.status==0) {
                                //将操作信息写入标签
                                $("#result").text(e.msg);
                                $('#example').modal('show');
                                $('#example').on('hide.bs.modal',function(){
                                    location.href = "{{url('bug/list')}}";
                                });
                            } else {
                                alert(e.msg);
                            }
                        },
                        error:function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log(errorThrown);
                        }
                    });
                });



                $('#addSave').on('click',function(){
                    //获取表单信息
                    var platform = $("select[name=platform]").val();
                    var error_type = $("input[name=error_type]").val();
                    var file = $("input[name=file]").val();
                    var error = $("input[name=error]").val();
                    var _remark = $("textarea[name=remark]").val();

                    var url = $("input[name=url]").val();
                    $.ajax({
                        type:"post",
                        url:"{{url('bug/do-bug-add')}}",
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        data:{platform:platform,error:error, error_type:error_type,file:file,remark:_remark,url:url},
                        dataType:'json',
                        success: function (e) {
                            if (e.status==0) {
                                $("#result").text(e.msg);
                                $('#example').modal('show');
                                $('#example').on('hide.bs.modal',function(){
                                    location.href = "{{url('bug/list')}}";
                                });
                            } else {
                                alert(e.msg);
                            }
                        },
                        error:function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log(errorThrown);
                        }
                    });

                });

                //点击搜索
                $('#keyword').on('click',function()
                {
                    seach();
                });

                function seach()
                {
                    var keywords = $("input[name=seach]").val();
                    var para = keywords.length > 0 ?  '?keywords='+keywords : '';
                    var url = "{{url('bug/list')}}"+para;
                    $('#keyword').prop('href', url);
                    location.href = url;
                }
            });
        </script>
    @endpush
@endsection

