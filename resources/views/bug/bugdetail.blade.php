@extends('layouts.master')
@section('content')
    @push('css')
    <link rel="stylesheet" href="{{asset('/bugsystem/plugins/datatables/dataTables.bootstrap.css')}}">
    @endpush
    {{--主体右上--}}
    <section class="content-header">
        <h1 id="examples">
            bug 管理
            <small>advanced tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!--主体内容-->
    <section class="content">
        <div class="row" id="app">
            <!--列表-->
            <div class="col-xs-12">
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">错误消息详情</h3>
                    {{--<div class="pull-left"><h3 class="box-title">@lang('leftnav.showTask')</h3></div>--}}
                        <button id="goback" type="button" class=" btn-info btn-flat">返回</button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <tr>
                                <th>URL</th>
                                <th>{{$bugInfo->url}}</th>
                            </tr>
                            <tr>
                                <th>错误信息</th>
                                <th>{{$bugInfo->error_message}}</th>
                            </tr>
                            <tr>
                                <th>错误文件</th>
                                <th>{{$bugInfo->file}} {{$bugInfo->line}} </th>
                            </tr>
                            <tr>
                                <td>错误详情</td>
                                <td>{{$bugInfo->trace}}</td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    @push('scriptfiles')
    <script src="{{asset('/bugsystem/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/bugsystem/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function(){
            $("#goback").on('click',function(){
                history.back();
            });
        });
    </script>
    @endpush
@endsection

