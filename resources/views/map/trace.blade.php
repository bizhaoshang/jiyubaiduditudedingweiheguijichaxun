@extends('layouts.master')
@section('content')
    
<!--定位--> 
 
     <section class="content-header">
       <div class="box row">
          
               <div class="margin">
                <div class="btn-group">
                  <a href="{{url('map/point?appid='.$appid)}}"  class="btn btn-{{$nav=='point'?'success':'default'}}">定位</a>       
                
                  <a href="{{url('map/trace?appid='.$appid)}}"  class="btn btn-{{$nav=='trace'?'success':'default'}}">轨迹</a>                 
                  <a  class="btn btn-default">导航1</a>             
               
                  <a  class="btn btn-default">导航2</a>
                 
                </div>
               
              </div>
          
       </div>

    </section>   


    <!--主体内容-->
    <section class="content">
    <div class="row">
        <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">最近七天轨迹数据</h3>

              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                @foreach($days as $k=>$d)
          
                 
                <!-- /.item -->
                <li class="item">                 
                  
                      <a href="?appid={{$appid}}&day={{$k}}">{{$k}}</a>                       
                 
                </li>
                @endforeach
             
                <!-- /.item -->
              </ul>
              <div class="form-group">
                

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                  <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat">查询</button>
                    </span>
                </div>
                <!-- /.input group -->
              </div>
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>
            
        </div>
        <div class="col-md-9"> 
            <div id="allmap"></div>
        </div>    
    </div>            
             
               
         
    </section>

    @push('scriptfiles')
    <script type="text/javascript">
        // 百度地图API功能
        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
        map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用
        var point = new BMap.Point({{$point->longi}},{{$point->lati}});
        var point_arr = [];
        map.centerAndZoom(point, 14);
        
       
        var data_info = [];
        @foreach($points as $p)
            var point = new BMap.Point({{$p->longi}}, {{$p->lati}});
            point_arr.push(point);
            var infoTxt = '经纬度:{{$p->longi}},{{$p->lati}}<br/>';
            infoTxt +='时速:{{$p->speed}}<br/>';
            infoTxt +='精度:{{$p->accuracy}}<br/>';
            infoTxt +='时间:{{$p->created_at}}<br/>';
            infoTxt +='acc数据:{{$p->acc}}<br/>';
            infoTxt +='地图地址:{{$p->address}}';
            var info = [{{$p->longi}},{{$p->lati}},infoTxt];
            data_info.push(info)
        @endforeach

        var opts = {
                width : 300,     // 信息窗口宽度
                height: 200,     // 信息窗口高度
                title : "位置信息" , // 信息窗口标题
                enableMessage:true//设置允许信息窗发送短息
               };

        var polyline = new BMap.Polyline(point_arr, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});   //创建折线
        map.addOverlay(polyline);   //增加折线


        for(var i=0;i<data_info.length;i++){

            var marker = new BMap.Marker(new BMap.Point(data_info[i][0],data_info[i][1]));  // 创建标注
            var content = data_info[i][2];
            map.addOverlay(marker);               // 将标注添加到地图中
            addClickHandler(content,marker);
        }
        function addClickHandler(content,marker){
            marker.addEventListener("click",function(e){
                openInfo(content,e)}
            );
        }
        function openInfo(content,e){
            var p = e.target;
            var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
            var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
            map.openInfoWindow(infoWindow,point); //开启信息窗口
        }


     //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    });
       
    </script>
    @endpush
@endsection

