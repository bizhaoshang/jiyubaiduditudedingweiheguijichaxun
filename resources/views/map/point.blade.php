@extends('layouts.master')
@section('content')
    
<!--定位--> 
 
    <section class="content-header">
       <div class="box row">
          
               <div class="margin">
                <div class="btn-group">
                  <a href="{{url('map/point?appid='.$appid)}}"  class="btn btn-{{$nav=='point'?'success':'default'}}">定位</a>       
                
                  <a href="{{url('map/trace?appid='.$appid)}}"  class="btn btn-{{$nav=='trace'?'success':'default'}}">轨迹</a>                 
                  <a  class="btn btn-default">导航1</a>             
               
                  <a  class="btn btn-default">导航2</a>
                 
                </div>
               
              </div>
          
       </div>
    </section>   
    <!--主体内容-->
    <section class="content">
       

              
               <div id="allmap"></div>
             
               
         
    </section>

    @push('scriptfiles')
    <script type="text/javascript">
        // 百度地图API功能
        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
        map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用
        var point = new BMap.Point({{$point->longi}},{{$point->lati}});
        var marker = new BMap.Marker(point);  // 创建标注
        map.addOverlay(marker);              // 将标注添加到地图中
        map.centerAndZoom(point, 14);
        var opts = {
          width : 300,     // 信息窗口宽度
          height: 200,     // 信息窗口高度
         
          
        }
        var infoTxt = '经纬度:{{$point->longi}},{{$point->lati}}<br/>';
            infoTxt +='时速:{{$point->speed}}<br/>';
            infoTxt +='精度:{{$point->accuracy}}<br/>';
            infoTxt +='时间:{{$point->created_at}}<br/>';
            infoTxt +='acc数据:{{$point->acc}}<br/>';
            infoTxt +='地图地址:{{$point->address}}';
        var infoWindow = new BMap.InfoWindow(infoTxt, opts);  // 创建信息窗口对象 
        marker.addEventListener("click", function(){          
            map.openInfoWindow(infoWindow,point); //开启信息窗口
        });

   
    </script>
    @endpush
@endsection

