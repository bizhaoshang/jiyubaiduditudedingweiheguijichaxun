<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>定位轨迹查询</title>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=5viKG8FFUlKS5HvoAWWrGyUWI0mOSGNN" ></script>
    <!-- Tell the browser to be responsive to screen width -->
    <style type="text/css">
    #allmap {width: 100%;height:800px;overflow: hidden;margin:0;font-family:"微软雅黑";float: left}
    </style>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
  
    @include('layouts.head')
    @stack('css')
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!--顶部导航-->

@include('layouts.topnav')

<!--顶部导航end-->
    <!--左侧-->
    <aside class="main-sidebar">

        @include('layouts.leftnav')

    </aside>
    <!--左侧end-->

    
    
      
        
   

    <div class="content-wrapper" >
    
        @section('content')
            content
        @show
    </div>


    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.8
        </div>
        <strong>Copyright &copy; {{date('Y',time())}} <a href="{{url('/')}}">定位轨迹查询系统</a>.</strong> All rights reserved.
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


@include('layouts.footer')


@stack('scriptfiles')
@stack('scripts')
</body>
</html>
