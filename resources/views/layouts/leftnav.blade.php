<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left">
                <a>您好： {{Auth::user()->username}}</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active treeview">
                <a href="{{url('/')}}">
                    <i class="fa fa-dashboard"></i> <span>工作台</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>设备信息</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
                </a>
                <ul class="treeview-menu">
               
                @if(isset($appids))
                    @foreach($appids as $v)
                        <li><a href="{{url('map/point?appid='.$v->appid)}}">
                        <i class="fa fa-circle-o"></i>AppId:{{$v->appid}}</a></li>
                    @endforeach
                @endif
                </ul>
            </li>
            <li class="treeview active ">
                <a href="{{url('user/info')}}">
                    <i class="fa fa-files-o"></i>
                    <span>修改信息</span>
                  
                </a>
                
            </li>
            
            
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
