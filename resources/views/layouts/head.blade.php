<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="shortcut icon" href="/img/li-logo.jpg"/>
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('/bugsystem/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{asset('/bugsystem/ionicons-master/css/ionicons.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('/bugsystem/dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('/bugsystem/dist/css/skins/_all-skins.min.css')}}">
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('/bugsystem/plugins/iCheck/flat/blue.css')}}">
<!-- Morris chart -->
<link rel="stylesheet" href="{{asset('/bugsystem/plugins/morris/morris.css')}}">

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('/bugsystem/plugins/datepicker/datepicker3.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>-->
{{--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>--}}
<script src="{{asset('/js/html5shiv.min.js')}}"></script>
{{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
<script src="{{asset('/js/respond.min.js')}}"></script>
<!--<![endif]-->