<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/**
 * | --------------------------------- |
 * |          首页路由组
 * | --------------------------------- |
 */
Route::get('/', 'HomeController@admin'); // 首页


Route::group(['prefix'=>'map'],function(){
    Route::get('point','MapController@point');//定位
    Route::get('trace','MapController@trace');//历史轨迹 
    Route::get('test','MapController@test');//历史轨迹         
});

    



//登录注册路由
Auth::routes();

Route::get('/home',       'HomeController@index');

Route::post('/send-email', 'HomeController@sendMail');

//注销登录
Route::get('logout', 'HomeController@logout'); //后台注销

//测试Mongodb
Route::get('mongo','MongodbController@index');