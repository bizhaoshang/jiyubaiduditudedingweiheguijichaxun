/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : mapgps

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-05-17 16:46:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_04_17_020212_create_bugs_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_04_17_020400_create_bug_status_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_04_17_020450_create_platforms_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_04_17_020541_create_platforms_users_table', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for traces
-- ----------------------------
DROP TABLE IF EXISTS `traces`;
CREATE TABLE `traces` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lati` varchar(50) DEFAULT NULL COMMENT '纬度',
  `longi` varchar(50) DEFAULT NULL COMMENT '经度',
  `speed` varchar(10) DEFAULT NULL COMMENT '速度',
  `accuracy` varchar(10) DEFAULT NULL,
  `u` varchar(20) DEFAULT NULL COMMENT '用户名（手机格式）',
  `p` varchar(20) DEFAULT NULL COMMENT '明文校验密码（非登录密码）',
  `acc` varchar(10) DEFAULT NULL COMMENT '充电状态',
  `appid` varchar(10) DEFAULT NULL COMMENT '用于区分同个用户名下不同的设备。',
  `address` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of traces
-- ----------------------------
INSERT INTO `traces` VALUES ('3', '24.99', '115.888', '15', '10', 'bzs', '3699', 'no', '222', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-17 08:58:27', '2017-05-17 08:58:27');
INSERT INTO `traces` VALUES ('4', '24.936245', '115.800', '15', '10', 'bzs', '3699', 'no', '333', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-17 09:07:38', '2017-05-17 09:07:38');
INSERT INTO `traces` VALUES ('5', '24.946245', '115.666', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-17 10:23:21', '2017-05-17 10:23:21');
INSERT INTO `traces` VALUES ('7', '24.950607745638', '115.67715809925', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-16 10:24:56', '2017-05-17 10:24:56');
INSERT INTO `traces` VALUES ('8', '24.955602053402', '115.67715863981', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-16 11:31:20', '2017-05-17 11:31:20');
INSERT INTO `traces` VALUES ('9', '24.890', '115.67716080707', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-09 11:40:52', '2017-05-17 11:40:52');
INSERT INTO `traces` VALUES ('10', '24.880', '115.666', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-17 11:40:52', '2017-05-17 11:40:52');
INSERT INTO `traces` VALUES ('11', '24.870', '115.667', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-17 11:40:52', '2017-05-17 11:40:52');
INSERT INTO `traces` VALUES ('12', '24.860', '115.668', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-17 11:40:52', '2017-05-17 11:40:52');
INSERT INTO `traces` VALUES ('13', '24.850', '115.669', '15', '10', 'bzs', '3699', 'no', '111', '江西省赣州市寻乌县G206(烟汕线)', '2017-05-17 11:40:52', '2017-05-17 11:40:52');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '0为普通用户。1为管理者',
  `true_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'api token',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('11', 'asan', '111111@asfsd.com3', '$2y$10$aw3SU1JGMBblxH8h6DMiu.NNKUuyRcyWeuU4CxRm6ZiRQ1muNYGzK', '0', '', 'PWsGLcfLbOJmwpGpbCkDyTeukaK3PNl6BH9mwhHGNSCYNmJA8BOqreVd0MYj', null, '2017-05-16 14:39:55', '2017-05-16 14:45:36', null);
INSERT INTO `users` VALUES ('2', 'liyong', '15210087900@163.com', '$2y$10$utqPu88Yb9GM1tRYpoJmCeuP631wFVrAByTjdSqeJwksRTHP8BZry', '0', '李勇', 'tEgutJBgpBhqvxLfu862FBona5WFJUCAyvBjsd5aiHM0f3gb72K2zv57X8Ss', null, '2017-04-19 10:17:03', '2017-05-06 04:17:22', null);
INSERT INTO `users` VALUES ('10', 'bzs', 'safsd@sd.com', '$2y$10$utJHwjP16ZR1YGzCZOaICuMovgK7u.lPRVVARthyVdgDl7.NSJFqm', '0', '', 'mL9Y76DGE19PiJnD185iCpLnMINsXXSILAS9vc9qywMNp7X2UN0JrEQXSJ4V', '3699', '2017-05-16 14:36:21', '2017-05-17 10:25:41', null);
